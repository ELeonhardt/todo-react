import React from 'react';
import styled from 'styled-components';


const OptionsField = styled.ul`
  background-color: ${props => props.theme.todoBackgroundColor};
  color: ${props => props.theme.optionsColor};
  /* font-size: 0.8rem; */
  padding: 1em;
  border-bottom-right-radius: 0.3em;
  border-bottom-left-radius: 0.3em;
  display: grid;
  grid-template-columns: repeat(3, 1fr);
  align-items: center;
  box-shadow: 10px 10px 20px ${props => props.theme.shadowColor};

  & > * {
      font-size: 0.8rem;
  }

  & > *:last-child {
      text-align: right;
  }

  form {
      ul {
        display: flex;
        justify-content: space-around;
      }
  }

  @media (max-width: 650px) {
      grid-template-columns: repeat(2, 1fr);
      padding: 0;
      background-color: ${props => props.theme.mainBackgroundColor};
      box-shadow: none;

      & > * {
          background-color: ${props => props.theme.todoBackgroundColor};
          padding: 1em;
      }

      form {
          grid-column: 1 / -1;
          margin-top: 1.5em;
          border-radius: 0.3em;
          box-shadow: 0 0 2em ${props => props.theme.shadowColor};
      }

      & > *:first-child {
          border-bottom-left-radius: 0.3em;
          height: 100%;
          box-sizing: border-box;
      }

      & > *:last-child {
          grid-row: 1;
          grid-column: 2;
          border-bottom-right-radius: 0.3em;
      }
  }
`;

const FilterItem = styled.li`

    font-weight: bold;
    label {
        cursor: pointer;
    }

    label:hover {
        color: ${props => props.theme.optionsHover};
    }

    input:checked + label {
        color: ${props => props.theme.optionsActive};
    }

    input[type="radio"] {
        display: none;
    }
`;

const ClearCompleted = styled.button`

  background-color: inherit;
  border: none;
  color: inherit;
  cursor: pointer;
  font: inherit;

  &:hover {
  color: ${props => props.theme.optionsHover};
  }

`;

class Options extends React.Component {

    handleChange = (e) => {
        this.props.changeFilter({status: `${e.target.id}`});
    }

    /*
    
    several calls to deleteTodo did not work since setState is a callback function and it wasn't updating the todos in between several calls.
    I worked around this by implementing a new deleteTodos function that eliminates all completed todos... 
    */

    render () {
        const activeTodos = this.props.todos.reduce((tally, todo) => (!todo.done ? tally+1 : tally),  0);
        return (
            <OptionsField>
                <li>{`${activeTodos}`} items left!</li>
                <form onChange={this.handleChange}>
                    <ul>
                        <FilterItem>
                            <input type="radio" defaultChecked name="filter" id="all" value="All" />
                            <label htmlFor="all">All</label>
                        </FilterItem>
                        <FilterItem>
                            <input type="radio" name="filter" id="active" value="Active" />
                            <label htmlFor="active">Active</label>
                        </FilterItem>
                        <FilterItem>
                            <input type="radio" name="filter" id="completed" value="completed" />
                            <label htmlFor="completed">Completed</label>
                        </FilterItem>
                    </ul>
                </form>
                <li><ClearCompleted onClick={() => this.props.deleteTodos()} data-testid="clear-completed">Clear completed</ClearCompleted></li>
            </OptionsField>

        )
    }
}

export default Options;