import React from 'react';
import styled from 'styled-components';

const StyledHeader = styled.div`
    
  display: flex;
  justify-content: space-between;
  align-content: center;

    h1 {
        color: ${props => props.theme.headerColor};
        font-weight: 700;
        text-transform: uppercase;
        font-size: 2rem;
        letter-spacing: 0.3em;
        margin: 1em 0;
    }

    .theme-changer {
        border: none;
        background: transparent;
        cursor: pointer;
    }

    /* img {
        width: 100%;
    } */
`;

class ListHeader extends React.Component {

    render() {
        return(
            <StyledHeader className="header">
                <h1>Todo</h1>
                <button className="theme-changer" onClick={this.props.changeTheme}>
                    <img alt="theme-changer" src={this.props.theme.themeChanger}>
                    </img>
                </button>
            </StyledHeader>
        )
    }

}

export default ListHeader;