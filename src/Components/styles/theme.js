// could I create a general theme and extend later to avoid duplication?

export const lightTheme = {
    mainBackgroundColor: 'hsl(0, 0%, 98%)',
    todoBackgroundColor: 'hsl(0, 0%, 100%)',
    mainBackground: 'url(/images/bg-desktop-light.jpg)',
    mobileBackground: 'url(/images/bg-mobile-light.jpg)',
    themeChanger: '/images/icon-moon.svg',
    headerColor: 'hsl(0, 0%, 100%)',
    optionsColor: 'hsl(236, 9%, 61%)',
    optionsHover: 'hsl(236, 9%, 35%)',
    optionsActive: 'hsl(220, 98%, 61%)',
    todoTextColor: 'hsl(235, 19%, 35%)',
    todoTextLineThrough: 'hsl(236, 9%, 61%)',
    shadowColor: 'hsl(236, 33%, 92%)',
    borderColor: 'hsl(236, 33%, 92%)',

}

export const darkTheme = {
    mainBackgroundColor: 'hsl(235, 21%, 11%);',
    todoBackgroundColor: 'hsl(235, 24%, 19%);',
    mainBackground: 'url(/images/bg-desktop-dark.jpg)',
    mobileBackground: 'url(/images/bg-mobile-dark.jpg)',
    themeChanger: '/images/icon-sun.svg',
    headerColor: 'hsl(0, 0%, 100%)',
    optionsColor: 'hsl(233, 14%, 35%)',
    optionsHover: 'hsl(234, 39%, 85%);',
    optionsActive: 'hsl(220, 98%, 61%)',
    borderColor: 'hsl(233, 14%, 35%)',
    todoTextColor: 'hsl(234, 39%, 85%)',
    todoTextLineThrough: 'hsl(233, 14%, 35%)',
    shadowColor: 'hsl(0, 0%, 0%)',
}