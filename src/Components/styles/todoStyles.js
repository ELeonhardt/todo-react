import styled from 'styled-components';

export const CustomCheckbox = styled.div`

  position: relative;
  height: 1.2em;
  width: 1.2em;

  label {
    border: 1px solid ${props => props.theme.borderColor};;
    border-radius: 50%;
    cursor: pointer;
    height: 1.2em;
    left: 0;
    position: absolute;
    top: 0;
    width: 1.2em;

    display: flex;
    align-items: center;
    justify-content: center;
  }

  label:hover {
    border-color: ${props => props.theme.optionsHover};
  }

  label img {
    opacity: 0;
    width: 55%;
  }

  input[type="checkbox"] {
    visibility: hidden;
  }

  input[type="checkbox"]:checked + label {
    background: linear-gradient(120deg, #57ddff, #c058f3);
    border: none;
  }

  input[type="checkbox"]:checked + label img {
    opacity: 1;
  }

`;

export const TodoBox = styled.li`
  background-color: ${props => props.theme.todoBackgroundColor};
  padding: 1em;
  grid-gap: 1em;
  display: grid;
  grid-template-columns: auto minmax(0, 1fr) auto;;
  align-items: center;
  justify-content: center;
  border-bottom: 1px solid ${props => props.theme.borderColor};

  input {
    font: inherit;
    background: inherit;
    border: none;
    color: ${props => props.done ? props.theme.todoTextLineThrough : props.theme.todoTextColor};
    text-decoration: ${props => props.done && 'line-through'};
  }

  @media (max-width: 650px) {
    font-size: 0.8rem;
  }
`;

