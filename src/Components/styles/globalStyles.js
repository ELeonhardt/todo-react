
import { createGlobalStyle } from 'styled-components';
 
const GlobalStyle = createGlobalStyle`
  * {
    margin: 0;
    padding: 0;
    box-sizing: border-box;
  }

    *, *:before, *:after {
        box-sizing: inherit;
    }

    body {
        font-family: "Josefin Sans", sans-serif;
        line-height: 1.5;
        background-color: ${({ theme }) => theme.mainBackgroundColor};
        min-height: 100vh;
        overflow: none;
    }

    a {
        text-decoration: none;
    }

    li {
        list-style: none;
    }

    input:focus,
    input:active,
    button:focus,
    button:active {
        outline: none;
        border: none;
    }
`;
 
export default GlobalStyle;