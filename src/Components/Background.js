import React from 'react';
import InputForm from './InputForm';
import ToDoList from './ToDoList';

class Background extends React.Component {
    render () {
        return (
        
                <div className="wrapper">
                    <div className="header">
                        <h1>Todo</h1>
                        <button className="theme-changer">
                            <img src="/images/icon-sun.svg" alt="theme-changer"></img>
                        </button>
                    </div>
                    <InputForm addTodo={this.props.addTodo}/>
                    <ToDoList todos={this.props.todos} />
                </div>
        )
    }
}

export default Background;