import React from 'react';
import styled, { ThemeProvider} from 'styled-components';
import { DragDropContext, Droppable, Draggable } from 'react-beautiful-dnd';
import GlobalStyle from './styles/globalStyles';
import { lightTheme, darkTheme } from './styles/theme';
import InputForm from './InputForm';
import ToDo from './ToDo';
import Options from './Options';
import ListHeader from './ListHeader';

const TodoWrapper = styled.div`
    max-width: 600px;
    position: relative;
    top: 3rem;
    margin: auto;

    @media (max-width: 650px) {
        padding: 0 2rem;
        top: 1.5rem;
    }
`;

const TodoList = styled.ul`

    box-shadow: 10px 10px 20px ${props => props.theme.shadowColor};
    div:first-child li{
      border-top-left-radius: 0.3em;
      border-top-right-radius: 0.3em;
    }

    @media (max-width: 650px) {
        box-shadow: none;
    }
`;

const BackgroundHeader = styled.div`
  width: 100vw;
  position: absolute;
  top: 0;
  height: 30vh;
  background: ${props => props.theme.mainBackground} center center/cover;

  @media (max-width: 650px) {
      content: ${props => props.theme.mobileBackground};
      max-width: 100vw;
  }
`;

const Footer = styled.footer`
    display: flex;
    justify-content: center;
    align-items: center;
    text-align: center;
    padding: 1.5em 0;
    color: ${props => props.theme.optionsColor};
`;


class App extends React.Component {

// define the empty state:
state = {
    todos: [],
    filter: {
        status: 'all',
    },
    currentTheme: darkTheme,
};


// method to add a todo item to the list:
    addTodo = todo => {
        // copy the state
        const newTodos = [...this.state.todos];
        // add my item to the copied state
        newTodos.unshift(todo);
        // set the state with the state API
        this.setState({todos: newTodos});
    }

    // change a todo-item
    changeTodo = todo => {
        // copy the state
        const newTodos = [...this.state.todos];
        // update the item in question
        const myIndex = newTodos.findIndex(item => item.key === todo.key);
        newTodos[myIndex] = todo;
        this.setState({ todos: newTodos});

    }

    deleteTodos = () => {
        const newTodos = [...this.state.todos];
        const uncomplete = newTodos.filter(item => item.done === false);
        this.setState( {todos: uncomplete});
    }

    deleteTodo = todo => {
        const newTodos = [...this.state.todos];
        const myIndex = newTodos.findIndex(item => item.key === todo.key)
        newTodos.splice(myIndex, 1);
        this.setState({ todos: newTodos});
    }

    changeFilter = filterObject => {
        // for now, I simply want to override the filter, but this could change
        this.setState({filter: filterObject});
    }

    methods = {
        active: (array) => array.filter(todo => todo.done===false),
        completed: (array) => array.filter(todo => todo.done === true),
        all: (array) => { return array;},
    }

    applyListFilter = () => {
        return this.methods[this.state.filter.status](this.state.todos);
    }

    changeTheme = () => {
        if(this.state.currentTheme === darkTheme) {
            this.setState({currentTheme: lightTheme});
        }
        else {
            this.setState({currentTheme: darkTheme});
        }
    }

    handleOnDragEnd = (result) => {
        // avoid application error if item is dragged outside of the dragdropcontext
        if(!result.destination) return;
        // copy the state
        const reorderedTodos = [...this.state.todos];
        // obtain the todo and remove it from the array
        const [movedTodo] = reorderedTodos.splice(result.source.index, 1);
        reorderedTodos.splice(result.destination.index, 0, movedTodo);
        this.setState({todos: reorderedTodos});

    }

    render () {
        return (
            <ThemeProvider theme={this.state.currentTheme}>
                    <GlobalStyle />
                    <BackgroundHeader></BackgroundHeader>
                    <TodoWrapper>
                        <ListHeader changeTheme={this.changeTheme} theme={this.state.currentTheme}/>
                        <InputForm addTodo={this.addTodo}/>
                        <DragDropContext onDragEnd={this.handleOnDragEnd}>
                            <Droppable droppableId="todos">
                                {(provided) => (
                                <TodoList {...provided.droppableProps} ref={provided.innerRef}>
                                    {this.applyListFilter().map((todo, index) => 
                                    <Draggable key={todo.key} draggableId={todo.key.toString()} index={index}>{(provided)=>(
                                    <div ref={provided.innerRef} {...provided.draggableProps} {...provided.dragHandleProps}><ToDo todo={todo} changeTodo={this.changeTodo} deleteTodo={this.deleteTodo}/></div>)}</Draggable>)}
                                    {provided.placeholder}
                                </TodoList>
                                )}
                            </Droppable>
                        </DragDropContext>
                        <Options changeFilter={this.changeFilter} todos={this.state.todos} deleteTodos={this.deleteTodos}/>
                        <Footer>
                        <p>Drag and drop to reorder list</p>
                    </Footer>
                    </TodoWrapper>
            </ThemeProvider>
        )
    }
}

export default App;