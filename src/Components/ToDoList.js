import React from 'react';
import ToDo from './ToDo';

class ToDoList extends React.Component {

    todoList = [...this.props.todos]
    methods = {
        active: () => this.todoList.filter(todo => todo.done===false),
        completed: () => this.todoList.filter(todo => todo.done === true),
        all: () => { return this.todoList;},
    }
    
    filterUndone = () => {
        const undone = this.props.todos.filter( todo => todo.done === false);
        console.log(undone);
    }

    filterDone = () => {
        const done = this.props.todos.filter( todo => todo.done === true);
        console.log(done);
    }

    changeList = (e) => {
        this.todoList = this.methods[e.target.id];
    }
    
    render () {
        return (          
            <ul>
                {this.props.todos.map( todo => <ToDo key={todo.key} todo={todo} />)}
            </ul>
        )
    }
}

export default ToDoList;