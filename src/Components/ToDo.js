import React from 'react';
import styled from 'styled-components';
import { CustomCheckbox, TodoBox } from './styles/todoStyles';

const CancelButton = styled.button`
  border: none;
  background: inherit;
  position: relative;
  height: 1.5em;
  width: 1.5em;
  cursor: pointer;
`;

class ToDo extends React.Component {
    handleChange = event => {
        // 1. get current state and copy it
        const updatedTodo = { ...this.props.todo};
        // 2. update the copy of the current state
        if(event.target.type === 'checkbox'){
            updatedTodo.done = event.target.checked;
        }
        else {
            updatedTodo[event.target.name] = event.target.value;
        }
        // 3. write the update to state
        this.props.changeTodo(updatedTodo);
    }
    
    render () {
        return (
            <TodoBox done={this.props.todo.done}>
                <CustomCheckbox>
                    <input type="checkbox" name={this.props.todo.key} id={this.props.todo.key} checked={this.props.todo.done} data-testid="todo-checkbox" onChange={this.handleChange}/>
                    <label htmlFor={this.props.todo.key}>
                        <img src="/images/icon-check.svg" alt=""/>
                    </label>
                </CustomCheckbox>
                <input type="text" name="description" id="todo" data-testid="todo-description" value={this.props.todo.description} onChange={this.handleChange}/>
                <CancelButton data-testid ="cancel-todo" onClick={() => this.props.deleteTodo(this.props.todo)}>
                    <img src='images/icon-cross.svg' alt="cancel"></img>
                </CancelButton>
            </TodoBox>
        )
    }
}

export default ToDo;