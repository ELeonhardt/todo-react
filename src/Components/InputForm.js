import React from 'react';
import styled from 'styled-components';
import { CustomCheckbox } from './styles/todoStyles';

const InvisibleSubmit = styled.button`
    visibility: hidden;
`;

// there is little difference between the TodoBox and this - can I maybe extend the todoBox styles somehow?
const StyledInputForm = styled.form`
    background-color: ${props => props.theme.todoBackgroundColor};
    padding: 1em;
    grid-gap: 1em;
    display: grid;
    grid-template-columns: auto minmax(0, 1fr) auto;
    align-items: center;
    justify-content: center;
    margin-bottom: 1.5em;
    border-radius: 0.3em;

    input {
        font: inherit;
        background: inherit;
        border: none;
        color: ${props => props.theme.todoTextColor};
  }

  @media(max-width: 650px) {
    font-size: 0.8rem;
  }
`;

class InputForm extends React.Component {

    textRef = React.createRef();
    doneRef = React.createRef();
    
    createTodo = event => {
        event.preventDefault();
        const todo = {
            key: Date.now(),
            description: this.textRef.current.value,
            done: this.doneRef.current.checked,
        }
        this.props.addTodo(todo);
        event.currentTarget.reset();
    }
    
    render () {
        return (
            <StyledInputForm onSubmit={this.createTodo}>
                <CustomCheckbox>
                    <input type="checkbox" name="done" id="done" ref={this.doneRef} />
                    <label htmlFor="done">
                        <img src="/images/icon-check.svg" alt=""/>
                    </label>
                </CustomCheckbox>
                <input type="text" name="todo" id="todo" ref={this.textRef} data-testid="todo-input"/>
                <InvisibleSubmit type="submit"></InvisibleSubmit>
          </StyledInputForm>

        )
    }
}

export default InputForm;