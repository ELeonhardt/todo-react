import React from 'react'
import { debug, fireEvent, render, screen } from '@testing-library/react'
import ToDoList from '../ToDoList'


const todos = [
    {
        key: 1,
        done: true,
        description: 'something to do',
    },
    {
        key: 2,
        done: false,
        description: 'something else to do',
    }
]

test('<ToDoList />', () => {
    render(<ToDoList todos={todos}></ToDoList>);
    // check it actually renders all the todos in the list
    expect(screen.getAllByTestId('todo-description').length).toBe(todos.length);
    // all the other functionalities will be tested in the App component
})