import React from 'react'
import { render, fireEvent } from '@testing-library/react'
import userEvent from '@testing-library/user-event'
import ToDo from '../ToDo'

const todo =  {
        done: false,
        description: 'something to do'
    }

const changeTodo = jest.fn();
const deleteTodo = jest.fn();


    // confirm that the input can be changed
    test('<ToDo />', () => {
        // confirm that the component renders and gets the data from the todo
        const { queryByTestId, getByTestId } = render(<ToDo todo={todo} changeTodo={changeTodo} deleteTodo={deleteTodo}></ToDo>);
        expect(queryByTestId('todo-description').value).toBe(todo.description);
        expect(queryByTestId('todo-checkbox').checked).toBe(todo.done);
        // confirm that clicking both buttons calls the corresponding functions
        fireEvent.click(queryByTestId('todo-checkbox'));
        expect(changeTodo).toBeCalled();
        fireEvent.click(queryByTestId('cancel-todo'));
        expect(deleteTodo).toBeCalledWith(todo);
        // confirm that the input update triggers the necesary functions
        todo.description = 'something different to do!';
        userEvent.type(getByTestId('todo-description'), todo.description);
        expect(changeTodo).toBeCalledTimes(todo.description.length + 1);
    })