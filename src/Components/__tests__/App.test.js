import React from 'react'
import { screen, render } from '@testing-library/react'
import userEvent, { specialChars } from '@testing-library/user-event'
import App from '../App';

test('<App /> crud tests', () => {
    render(<App />);
    // create
    const todo = 'first todo'
    userEvent.type(screen.getByTestId('todo-input'), `${todo}${specialChars.enter}`)

    // read
    expect(screen.getByTestId('todo-description').value).toBe(todo);

    // update: modify text
    const addedText = ': get some coffee!';
    userEvent.type(screen.getByTestId('todo-description'), `${addedText}${specialChars.enter}`);
    expect(screen.getByTestId('todo-description').value).toBe(`${todo}${addedText}`);
    
    // update: mark as done
    expect(screen.getByTestId('todo-checkbox').checked).toBe(false);
    userEvent.click(screen.getByTestId('todo-checkbox'));
    expect(screen.getByTestId('todo-checkbox').checked).toBe(true);

    // delete
    userEvent.click(screen.getByTestId('cancel-todo'));
    expect(screen.queryAllByTestId('todo-description')).not.toBe();

})

test('<App /> Options tests', () => {
    render(<App />);
    // add several items to the list
    userEvent.type(screen.getByTestId('todo-input'), `task 1${specialChars.enter}`)
    userEvent.type(screen.getByTestId('todo-input'), `task 2${specialChars.enter}`)
    userEvent.type(screen.getByTestId('todo-input'), `task 3${specialChars.enter}`)
    userEvent.type(screen.getByTestId('todo-input'), `task 4${specialChars.enter}`)
    userEvent.type(screen.getByTestId('todo-input'), `task 5${specialChars.enter}`)

    // check to see if they all appear
    expect(screen.getAllByTestId('todo-description').length).toBe(5);
    // check if the items left corresponds to the real number of todos
    expect(screen.queryByText('5 items left!')).toBeTruthy();
    
    //mark 2 items as done and check the items left text again
    userEvent.click(screen.getAllByTestId('todo-checkbox')[0]);
    userEvent.click(screen.getAllByTestId('todo-checkbox')[1]);
    expect(screen.queryByText('3 items left!')).toBeTruthy();

    // click on the filters and check if the amount of items rendered is correct
    userEvent.click(screen.getByLabelText('Completed'));
    expect(screen.queryAllByTestId('todo-description').length).toBe(2);
    
    userEvent.click(screen.getByLabelText('Active'));
    expect(screen.queryAllByTestId('todo-description').length).toBe(3);
    
    userEvent.click(screen.getByLabelText('All'));
    expect(screen.queryAllByTestId('todo-description').length).toBe(5);

    // check if the clear completed is working properly
    userEvent.click(screen.getByTestId('clear-completed'));
    expect(screen.getAllByTestId('todo-description').length).toBe(3);

    expect(true).toBeFalsy();
})